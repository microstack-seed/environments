# environmnt

This contains scripts and settings for setting up the hosted environment.

These scripts were written to setup a vanilla CentOS 8 server with latest security updates, the docker engine, and [Portainer.io](https://www.portainer.io/), a light-weight Docker UI for simple and easy management of your application stacks.

There are two scripts because we need a reboot step to ensure everything come ups automatically. Script 2 are post boot commands to check Docker Process is autostarting and [installs Portainer.io](https://www.portainer.io/installation/)