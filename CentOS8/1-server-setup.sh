#!/usr/bin/env bash

#update repos
sudo yum update -y

#upgrade packages
sudo yum upgrade -y

#reference for docker CE install - https://docs.docker.com/install/linux/docker-ce/centos/#set-up-the-repository

#Make sure there are no legacy docker installs
sudo yum remove docker docker-common docker-selinux docker-engine -y

#Provision the docker repository
sudo yum install -y yum-utils device-mapper-persistent-data lvm2

#add the docker repo
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo -y

# install docker compose from repo
sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose && \
sudo chmod +x /usr/local/bin/docker-compose

#Install Containerd.io manually due to restriction on Centos8
sudo dnf install https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm -y

#install docker CE
sudo yum install docker-ce docker-ce-cli -y

#start the docker daemon
sudo systemctl start docker

#enable the docker daemon
sudo systemctl enable docker

#Add udigadmin to the docker group so you can run docker command without sudo
sudo usermod -a -G docker centos

echo "Give the system a few minutes to reboot, then run script 2"

#reboot so we can confirm the docker service start on boot
sudo shutdown -r now