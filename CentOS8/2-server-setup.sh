#!/usr/bin/env 

#after logging back in confirm docker is running. The following command should return an empty table (assuming you didn't load any containers but shouldn't return any errors
docker ps

#create portainer volume
docker volume create portainer_data

#launch portainer
docker run --name portainer --restart always -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data --label "com.centurylinklabs.watchtower.enable=true" portainer/portainer